var config = require('./../../config');
var path = require("path");
var AWS = require('aws-sdk');
var fs = require('fs');
var users = require('./../models').users;
var messages = require('./../models').messages;

function S3Upload() {}

S3Upload.generateHash = function (fileName, userId) {
    var cryptoKey = Date.now().toString();
    var crypto = require('crypto')
        , text = fileName + userId
        , key = cryptoKey
        , hash;

    hash = crypto.createHmac('sha1', key).update(text).digest('hex') + path.extname(fileName);
    return hash;
};

S3Upload.handleUpload = function (hash, filePath, recipient, room, userId, messageId, done) {

    AWS.config.update(config.s3);
    var s3 = new AWS.S3();

    s3.putObject({
        Bucket: 'ed-admin',
        Key: 'uploads/' + hash.fileData.url,
        Body: fs.createReadStream(filePath),
        ACL: 'public-read'
    }, function (err, resp) {

         hash.fileData.url =  config.s3.baseUrl + hash.fileData.url;

        var body = hash;

        messages.updateMessage(messageId, JSON.stringify(body), 2, function(err, success) {
            users.getById(userId, function(err, result) {

                var messageModel = {
                    id: messageId,
                    body: body,
                    roomId: room,
                    author: result,
                    createdAt: new Date().getTime(),
                    recipient_id: recipient,
                    is_file: 2
                };

                var io = require('../../app').get('io');

                if(recipient) {
                    io.sockets.in('user:'+userId).emit('chat.message.private', messageModel);
                    io.sockets.in('user:'+recipient).emit('chat.message.private', messageModel);
                }
                if(room) {
                    io.sockets.in(room).emit('chat.message', messageModel);
                }

                if(!room && !recipient) {
                     messageModel = {
                        id: success.insertId,
                        body: body,
                        roomId: null,
                        author: result,
                        createdAt: new Date().getTime(),
                        recipient_id: null,
                        is_file: 2
                    };
                    io.sockets.emit('chat.message', messageModel);
                }
            })
        });
        done(err, body);
    });
};


module.exports = S3Upload;