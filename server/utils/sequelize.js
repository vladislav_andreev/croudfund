var sequelizeInstance = require('sequelize');
var sequelize = new sequelizeInstance('croudFund', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 100,
        min: 5,
        idle: 10000
    },

    // SQLite only
    storage: 'path/to/database.sqlite'
});

module.exports = sequelize;