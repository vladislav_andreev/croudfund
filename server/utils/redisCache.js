var config = require('./../../config');
var redis = require('redis');
var client = redis.createClient(config.redis.port, config.redis.host, config.redis.options || {});
var shortId = require('shortid');
var async = require('async');
var messages = require('./../models').messages;
var uuid = require('uuid');

function RedisCache () {}

//Save data in redis storage
RedisCache.store = function (data, callback) {

    var is_read = data.is_read ? data.is_read : null;
    var newMessageId = uuid.v1();
    console.log(newMessageId);
    //Todo: remove crutch when client is ready and store timestamp
    /*var currentDateTime = new Date().toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '');*/
    var currentDateTime = new Date();
    var month = currentDateTime.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = currentDateTime.getDate();
    day = (day < 10 ? "0" : "") + day;


        currentDateTime = currentDateTime.getFullYear() + '-' + month + '-' + day + ' '+
        currentDateTime.getHours() + ':' + currentDateTime.getMinutes() + ':' + currentDateTime.getSeconds();


    client.hmset(newMessageId,
        'body', data.body,
        'roomId', data.roomId,
        'author', data.author,
        'recipient_id', data.recipient_id,
        'is_file', data.is_file,
        'createdAt', currentDateTime,
        'is_read', is_read
        //'id', newMessageId
    );

    callback();
};

//Move data into mysql
RedisCache.move = function () {

    var messagesData = [];
    var redisGetall = function(redisKey, callback) {
        return client.hgetall(redisKey, function (err, results) {
            var roomId = null;
            var recipientId = null;

            if(results.roomId != 'null') {
                roomId = results.roomId.toString();
            }

            if(results.recipient_id != 'null') {
                recipientId = results.recipient_id.toString();
            }

            messagesData.push([
                results.body.toString(),
                results.author.toString(),
                roomId,
                recipientId,
                parseInt(results.is_file),
                results.is_read.toString(),
                results.createdAt.toString()
            ]);
            callback();
        });
    };

    client.keys('*', function (err, keys) {

        if(! keys.length) {
            console.log('redis storage is empty!');
            return false;
        }

        async.each(keys, redisGetall, function() {
            client.del(keys, function(err){

                if(err) {
                    console.log(err);
                }

                messages.batchInsert(messagesData, function (err, done) {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log(done);
                        }

                    }
                );

            });

        });

    });
};

module.exports = RedisCache;