var config = require('./../../config');
var log = require('./logger');
var redis = require("redis");

//will be created on every server instances
var _redisPubClient = redis.createClient(config.redis.port, config.redis.host, config.redis.options || {});
var _redisSubClient = redis.createClient(config.redis.port, config.redis.host, config.redis.options || {});

var redisClients = {
    pubClient: _redisPubClient,
    subClient: _redisSubClient
};

module.exports = redisClients;