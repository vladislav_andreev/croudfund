var TokenStrategy = require('passport-token-auth').Strategy;
var users = require('./../models').users;

module.exports = function (passport) {
    passport.use(new TokenStrategy({
                passReqToCallback: true
            },
        function (req, token, done) {
            users.getUserByAccessToken(token, function (err, result) {
                if (err) {
                    return done(err)
                }

                if (!result) {
                    var error = new Error();
                    error.message = 'Invalid token';
                    error.status = 401;
                    return done(error);
                }
                var currentTimestamp = new Date().getTime();
                if (req.url !== '/api/v1/token' && result.token.data.expiresAt < currentTimestamp) {
                    var error = new Error();
                    error.message = 'Your token is expired; try to refresh your accessToken using refreshToken';
                    error.status = 403;
                    return done(error);
                }

                var user = result.user.data;
                done(null, user);
            })
        })
    )
};


