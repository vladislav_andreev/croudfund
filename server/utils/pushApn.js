var path = require('path');
var notify = require('push-notify');
var log = require('./../logger');
var users = require('./../models').users;

function PushApn() {

    this.apnDev = new notify.apn({
        cert: path.join(__dirname, '../../crt/aps_development.cer'),
        key: path.join(__dirname, '../../crt/live_dev.pem'),
        passphrase: 'silentfitness',
        gateway:'gateway.sandbox.push.apple.com'
    });

    this.apnProd = new notify.apn({
        cert: path.join(__dirname, '../../crt/ck_production.pem'),
        key: path.join(__dirname, '../../crt/ck_production.key'),
        passphrase: 'silentfitness',
        gateway: 'gateway.push.apple.com'
    });


    this.apnDev.on('transmissionError', function(errorCode, notification, device){
        log.error('push apn DEV transmission error: ' + errorCode, notification);
    });

    this.apnDev.on('transmitted', function(data){
        log.info('transmitted DEV: ', data);
    });

    this.apnProd.on('transmissionError', function(errorCode, notification, device){
        log.error('push apn PROD transmission error: ' + errorCode, notification);
    });

    this.apnProd.on('transmitted', function(data){
        log.info('transmitted PROD: ', data);
    });
}

PushApn.prototype.pushNotification = function(token, text, badge){
    var _this = this;
    //users.getDeviceToken(userId, function (err, result) {
    //    if (err) {
    //        log.error(err)
    //    } else {
    //        if (!result) {
    //            log.error('user not found');
    //        } else {
    //            var token = result.device_token;
                var data = {
                    token: token,
                    alert: {
                        "body": text
                    },
                    badge: badge,
                    sound: 'default',
                    expiry: Math.floor(Date.now() / 1000) + 604800
                };
                _this.apnDev.send(data);
                //_this.apnProd.send(data);

                log.info('pushNotification: ', data);
    //        }
    //    }
    //})

};

//module.exports.notify = new PushApn();
var apns = new PushApn();

apns.pushNotification('123', 'Live app push test', 1);