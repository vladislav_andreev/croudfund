var passport = require('passport');
var Strategy = require('passport-http-bearer').Strategy;
var userModel = require('../models').users;
var crypt = require('../utils/crypt');

var passportHandle = passport.use(new Strategy(
    function(token, cb) {
        token = crypt.decrypt(token);
        userModel.getById(JSON.parse(token).userId, function(err, user) {
            if (err) { return cb(err); }
            if (!user) { return cb(null, false); }
            return cb(null, user);
        });
    }));

module.exports = passportHandle;