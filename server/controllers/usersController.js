var users = require('./../models').users;
var validator = require('./../utils/bodyValidator');
var config = require('./../../config');

function getAll(req, res, next) {
    users.findAll({
        limit: req.query.limit,
        offset: req.query.offset
    })
    .then(function(user) {
        res.send(user);
    });
}

function addUser(req, res, next) {
    users.create(req.body)
        .then(function(data) {
           res.send(data);
        })
        .catch(function (error) {
            res.status(400);
            res.send(error);
        })
}

var getAllUsers = [
    getAll
];

var insert = [
    addUser
];

module.exports = {
    allusers: getAllUsers,
    insert: insert
};