var controllers = require('../controllers');
var passport = require('../utils/passportWrapper');

module.exports = function (app) {
    //static links
    app.get('/', controllers.static.index);
    app.get('/api/doc', controllers.static.swagger);
    app.get('/api', controllers.static.swagger);

    //users section
    app.route('/api/users')
        .get(controllers.users.allusers)
        .post(controllers.users.insert);

    //authentication....
    app.use(passport.authenticate('bearer', { session: false }));

};