var SequelizeBase = require('sequelize');
var sequelize = require('./../utils/sequelize');

var Users = sequelize.define('users', {

    email: {
        type: SequelizeBase.STRING,
        field: 'email',
        unique: true,
        validate:  {
            isEmail: true
        }
    },
    firstName: {
        type: SequelizeBase.STRING,
        field: 'firstName'
    },
    lastName: {
        type: SequelizeBase.STRING,
        field: 'lastName'
    }
}, {
    freezeTableName: true,
    timestamps: false
});

module.exports = Users;