var request = require('request');
var async = require('async');
var config = require('./../config');
//fill database with 50k random users 500 iterations 100 users each
var arr = new Array(500);

async.eachSeries(arr, function (i, cb) {
    request.get('http://api.randomuser.me/?results=100', function (err, result) {
        var body = JSON.parse(result.body);

        var jsonBody = body.results.map(function (item) {
            var user = item.user;
            user.id = user.md5.slice(-12);
            return {
                userId: user.id,
                firstname: user.name.first,
                lastname: user.name.last,
                knownas: user.username,
                siteid: user.salt,
                avatar: user.picture.thumbnail,
                password: require('crypto').randomBytes(8).toString('hex'),
                type: Math.ceil(10* Math.random())
            }
        });

        var jBody = {
            users: jsonBody
        };

        var options = {
            method: 'POST',
            url: config.server.baseURL + "api/user",
            headers: {
                'Content-Type': 'application/json',
                'api-key': 'uh8k497Hdk8jUJmhuf8jUUhdi8jcBYGhhjdjfhhuezz'
            },
            json: jBody
        };
        request(options, cb)
    })
}, function (err, result) {
    console.log(err, result);
    process.exit(0);
})

